const express = require('express');
const app = express();
const bb = require('express-busboy');
const cgibin = require('./build/Release/cgibin.node');
const server = require('http').Server(app);
const io = require('socket.io')(server);

app.use(express.static('public'));
bb.extend(app, {
    upload: true
});

app.post('/cgi-bin/gridlock.cgi', (req, res) => {
    res.status(200).send(cgibin.Gridlock(req.body));
});

app.post('/cgi-bin/test.cgi', (req, res) => {
    res.status(200).send(cgibin.Test(req.body));
});

app.post('/cgi-bin/dla.cgi', (req, res) => {
    const request = { ...req.body, ...req.files };
    res.status(200).send(cgibin.Dla(request));
});

io.on('connection', (socket) => {
    socket.on('sent', (data) => {
        socket.emit('received', 'Received: ' + data);
    });
});

app.use((req, res, next) => {
    res.status(404).send("404 Not Found!")
});

server.listen(3000);

{
  "targets": [
    {
      "target_name": "cgibin",
      "cflags!": [ 
        "-fno-exceptions"
      ],
      "cflags_cc!": [ 
        "-fno-exceptions"
      ],
      "sources": [
        "./src/gridlock/gridlock.c",
        "./src/gridlock/pieces.c",
        "./src/dla/DLA.c",
        "./src/dla/libbmp/bmpfile.c",
        "./src/index.cpp"
      ],
      "include_dirs": [
        "<!@(node -p \"require('node-addon-api').include\")"
      ],
      'defines': [ 'NAPI_DISABLE_CPP_EXCEPTIONS' ],
    }
  ]
}
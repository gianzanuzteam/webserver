#include <napi.h>
#include <string>
#include "gridlock/gridlock.h"
#include "dla/DLA.h"

std::string Header(void)
{
    std::string header;

	header.append("<!DOCTYPE html>");
	header.append("<html>");

	header.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/styles.css\" media=\"screen\" />");
	header.append("<link rel=\"shortcut icon\" href=\"/favicon.ico\" type=\"image/x-icon\">");
	header.append("<link rel=\"icon\" href=\"/favicon.ico\" type=\"image/x-icon\">");
	header.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");

    return header;
}

std::string Footer(void)
{
    std::string footer;
	footer.append("<footer><p>");
		footer.append("Desenvolvido por Giancarlo Zanuz.<br>");
		footer.append("<a href=\"mailto:gianzanuz@gmail.com\"> gianzanuz@gmail.com</a><br>");
		footer.append("Todos os direitos reservados.<br>");
	footer.append("</p></footer>");

    footer.append("</html>");

    return footer;
}

Napi::String Test(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();
    Napi::Object object = info[0].As<Napi::Object>();

    std::string body;
    body.append(Header());
	
	body.append("<head>");
		body.append("<h1>Resultado do teste CGI</h1>");
	body.append("</head>");
	
	body.append("<body><div>");
		body.append("Resultado:<br>");
		body.append("<textarea rows=\"1\" cols=\"50\" readonly>");
			body.append(object.Get("test").ToString());
		body.append("</textarea>");
	body.append("</div></body>");
	
    body.append(Footer());

    return Napi::String::New(env, body);
}

Napi::String Gridlock(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();
    Napi::Object object = info[0].As<Napi::Object>();

    /* Obtém um vetor com as peças */
    std::vector<piece_info_t> pieceInfoVector;
    const std::vector<std::string> pieces = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O"};  
    const std::vector<std::string> piecesLower = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o"};
    for(auto piece : pieces)
    {
        /* Verifica se possui a peça */
        if(!object.HasOwnProperty(piece))
            continue;
        
        /* Para cada quantidade da peça */
        int pieceSize = object.Get(piece).ToNumber();
        for(int i=0; i<pieceSize; i++)
        {
            piece_info_t pieceInfo;
            pieceInfo.piece = (pieces_t) (piece.at(0) - 'A');
            pieceInfoVector.push_back(pieceInfo);
        }
    }

    std::string body;
	body.append(Header());
	
	body.append("<head>");
		body.append("<h1>Resultado do jogo Cilada</h1>");
	body.append("</head>");
	
    piece_stats_t stats;
    if(solve(pieceInfoVector.size(), &pieceInfoVector[0], &stats))
    {
        body.append("<body><div style=\"position: relative;\">");
            body.append("<img src=\"../img/base.png\" alt=\"base\"/>");
            for(int i=0; i<pieceInfoVector.size(); i++)
            {
                char buffer[500];
                sprintf(buffer, "<img src=\"../img/%s%d.png\" style=\"position: absolute; width: 200px; height: 200px; top: %dpx; left: %dpx;\"/>", piecesLower[pieceInfoVector[i].piece].c_str(), pieceInfoVector[i].rot, pieceInfoVector[i].row*100, pieceInfoVector[i].col*100);
                body.append(buffer);
            }
        body.append("</div></body>");
	}

    body.append("<p>inserts = " + std::to_string(stats.inserts) + "</p>");
    body.append("<p>interractions = " + std::to_string(stats.interractions) + "</p>");
    body.append("<p>removes = " + std::to_string(stats.removes) + "</p>");

	body.append(Footer());

    return Napi::String::New(env, body);
}

Napi::String Dla(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();
    Napi::Object object = info[0].As<Napi::Object>();

    /* Estrutura de controle */
    DLA_control_t dlaControl;
    memset(&dlaControl, 0, sizeof(dlaControl));

    /* Verifica se possui imagem inicial */
    if(object.Has("img"))
    {
        std::string filename = object.Get("img").ToObject().Get("file").ToString();
        if(DLA_imageLoader(&dlaControl, filename.c_str()) != 0)
            return Napi::String::New(env, "ERROR: Could not load image file.");
    }
    else
    {
        /* Matrix inicial vazia */
        dlaControl.lSize = object.Get("lineCount").ToNumber().Int32Value();
        dlaControl.cSize = object.Get("columnCount").ToNumber().Int32Value();
		dlaControl.matrix = (uint8_t*) malloc(dlaControl.lSize * dlaControl.cSize);
		if(!dlaControl.matrix)
            return Napi::String::New(env, "ERROR: Could not allocate enough memory.");
		memset(dlaControl.matrix, 0, dlaControl.lSize * dlaControl.cSize);
    }

    /* Carrega demais propriedades */
    dlaControl.elements = object.Get("elementCount").ToNumber().Int32Value();
    dlaControl.maxInteractions = object.Get("maxInteractions").ToNumber().Int32Value();

    std::string body;
	body.append(Header());

	body.append("<head>");
		body.append("<h1>Resultado DLA</h1>");
	body.append("</head>");
	
    if(DLA_init(&dlaControl))
    {
        body.append("<body>");
            body.append("<div>");

            /* Cria uma imagem do tipo bitmap a partir da matriz */
            DLA_imageCreator(&dlaControl, "public/img/dla.bmp");
            body.append("<img src=\"../img/dla.bmp\"/>");

            body.append("</div>");
        body.append("</body>");
	}

	body.append(Footer());

    /* Restaura memória */
    free(dlaControl.matrix);

    return Napi::String::New(env, body);
}

Napi::Object Init(Napi::Env env, Napi::Object exports)
{
    exports.Set(
        Napi::String::New(env, "Gridlock"),
        Napi::Function::New(env, Gridlock)
    );
    exports.Set(
        Napi::String::New(env, "Test"),
        Napi::Function::New(env, Test)
    );
    exports.Set(
        Napi::String::New(env, "Dla"),
        Napi::Function::New(env, Dla)
    );

    return exports;
}

NODE_API_MODULE(cgibin, Init)